from functools import wraps

def my_input(log_file):
       
    def my_decorator(func):
        @wraps(func)
 
        def logging(*args,**kwargs):
            ''' Logs calls to my_function.log'''
            with open(log_file, 'a') as log:
                log.write (f"Running function: {func.__name__}\n")
                log.write (f"And the sum is: {func(*args,**kwargs)[0]} + {func(*args,**kwargs)[1]} = {func(*args,**kwargs)[2]}\n")              
                log.write (f"Done running function: {func.__name__}\n")
        return logging

    return my_decorator
    

@my_input("my_function.log")
def calculate(x,y):
    '''Adds two numbers'''
    sum = x + y
    
    return x, y, sum

calculate(1,2)
